/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AVLTree.h
 * Author: vinicius
 *
 * Created on 31 de Maio de 2018, 09:01
 */

#ifndef AVLTREE_H
#define AVLTREE_H

#include <iostream>

#include "Node.h"

using namespace std;

/**
 * AVL Tree Class
 * 
 * Provides methods for creating and manipulating AVL trees. Works with nodes
 * whose key is of type int, but the class could be Easily adapted to work with
 * templates. Kept in this form for a greater didactics.
 */
class AVLTree {
public:

    /**
     * Default constructor
     */
    AVLTree();

    /**
     * Default destructor
     */
    virtual ~AVLTree();

    /**
     * Receives as input a vector containing nodes organizes its elements of 
     * according to the properties of an AVL Tree;
     * @param array of elements
     * @param size array
     */
    void AVLify(int array[], int size);

    /**
     * Clean the AVL tree
     */
    void Clear();

    /**
     * Returns the number of elements contained in the AVL tree
     * @return count
     */
    int Size();

    /**
     * Check if a given element is present in the tree, returning a pointer to
     * the searched element, if it is found
     * @param key
     * @return pointer to the searched element
     */
    Node* Search(int key);

    /**
     * Calls the method that inserts an element into the AVL tree. 
     * Accepting a key as parameter.
     * @param key
     */
    void Insert(int key);

    /**
     * Calls the method that removes an element from the AVL tree
     * @param key
     */
    bool Remove(int key);

    /**
     * Calls the method prints on screen all the elements contained in the AVL
     * tree.
     */
    void PrintAVLTree();

private:

    // Tree root node
    Node* root;

    // tree element count
    int count;

    /**
     * Private method that inserts an element into the AVL tree.
     * Accepting a key as parameter; a pointer passed by reference, and a
     * boolean passed by eference indicating whether the height of the tree
     * has changed.
     * @param key
     * @param &pA
     * @param &h
     */
    void insert(int key, Node* &pA, bool &h);

    /**
     * Simple right rotation.
     * Rotates the tree to the right after being unbalanced
     * @param &pA
     * @param &pB
     */
    void rotateLL(Node* &pA, Node* &pB);

    /**
     * Single rotation to the left.
     * Rotates the tree to the left after being unbalanced
     * @param &pA
     * @param &pB
     */
    void rotateRR(Node* &pA, Node* &pB);

    /**
     * Right double rotation
     * @param &pA
     * @param &pB
     * @param &pC
     */
    void rotateLR(Node* &pA, Node* &pB, Node* &pC);

    /**
     * Left double rotation
     * @param &pA
     * @param &pB
     * @param &pC
     */
    void rotateRL(Node* &pA, Node* &pB, Node* &pC);

    /**
     * Private method that removes an element from the AVL tree
     * @param key
     * @param &p
     * @param &h
     */
    bool remove(int key, Node* &p, bool &h);

    /**
     * Rebalance indicating that the left subtree has shrunk
     * @param &pA
     * @param &h
     */
    void BalanceL(Node* &pA, bool &h);

    /**
     * Rebalance indicating that the right subtree has shrunk
     * @param &pA
     * @param &h
     */
    void BalanceR(Node* &pA, bool &h);

    /**
     * 
     * @param q
     * @param r
     * @param h
     */
    void DelMin(Node* &q, Node* &r, bool &h);

    /**
     * Private method prints on screen all the elements contained in the AVL
     * tree, from a root node.
     * @param node
     */
    void print(Node* &node);

    /**
     * Prints the value of a node
     * @param node
     */
    void visit(Node* &node);

};

#endif /* AVLTREE_H */

