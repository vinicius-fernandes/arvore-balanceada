/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: vinicius
 *
 * Created on 31 de Maio de 2018, 08:57
 */

#include <iostream>
#include <cstdlib>

#include "AVLTree.h"

using namespace std;

// menu method scope
int menu();

/*
 * main method
 */
int main(int argc, char** argv) {

    AVLTree avl;
    Node* aux = NULL;
    int* arrayToInsert = NULL;
    char flag; // flag indicating end of execution or continuation
    int option; // menu option
    int key;

    // Job - Initializer Static AVL Tree
    avl.Insert(5);
    avl.Insert(4);
    avl.Insert(3);
    avl.Insert(2);
    avl.Insert(1);
    avl.Insert(50);
    avl.Insert(6);
    avl.Insert(7);
    avl.Insert(8);
    avl.Insert(9);
    avl.Insert(10);

    while (true) {

        option = menu();
        system("clear");

        switch (option) {

            case 1:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              SIZE               ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " ➸ SIZE: " << avl.Size() << endl;
                break;

            case 2:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              SEARCH             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " ➸ Enter the key to search: ";
                cin >> key;
                aux = avl.Search(key);

                if (aux != NULL) {
                    cout << "\n ➸ SEARCH: Key found" << endl;
                    cout << "\t ➸ KEY   = " << aux->key << endl;
                    cout << "\t ➸ LEFT  = " << ((aux->left) ? aux->left->key : 0) << endl;
                    cout << "\t ➸ RIGHT = " << ((aux->right) ? aux->right->key : 0) << endl;

                } else
                    cout << "\n ➸ SEARCH: Key not found" << endl;
                break;

            case 3:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              INSERT             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " ➸ Enter the key to insert: ";
                cin >> key;
                avl.Insert(key);
                cout << "\n ➸ INSERT: Key " << key << " inserted successfully" << endl;
                break;

            case 4:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              REMOVE             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " ➸ Enter the key: ";
                cin >> key;

                if (avl.Search(key) == NULL)
                    cout << "\n ➸ Key not found in the tree" << endl;
                else if (avl.Remove(key))
                    cout << "\n ➸ REMOVE: Key " << key << " removed successfully" << endl;
                else
                    cout << "\n ➸ REMOVE: The key " << key << " does not exist in the tree" << endl;
                break;

            case 5:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              PRINT             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "\n ➸ PRINT: ";
                avl.PrintAVLTree();
                cout << endl;
                break;

            case 6:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              CLEAR             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                avl.Clear();
                cout << "\n ➸ CLEAR: AVL Tree cleans with success" << endl;
                break;

            case 7:
                int size, num;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              AVLify             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "\n ➸ Enter the size of the array: ";
                cin >> size;
                cout << endl;

                arrayToInsert = new int[size];
                for (int i = 0; i < size; i++) {
                    cout << " ➸ array[" << i << "] = ";
                    cin >> num;
                    arrayToInsert[i] = num;
                }
                avl.AVLify(arrayToInsert, size);

                cout << "\n ➸ AVLify: Values inserted successfully" << endl;
                delete arrayToInsert;
                break;

            case 0:
                exit(0);
                break;

            default:
                cout << "\n ➸ Invalid option. PLease choose a valid option!" << endl;
                break;
        }

        cout << "\n\n ➸ Continue? (y / n):  ";
        cin >> flag;
        if (flag == 'n' || flag == 'N')
            exit(EXIT_SUCCESS);

    }

    return 0;
}

/**
 * @return which function of the AVL Tree the user wants to call.
 */
int menu() {
    int option = 0;

    system("clear");
    cout << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                          AVL BALANCED TREE                     ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐\t" << "                                                         ▐" << endl;
    cout << "▐\t" << "[ 1 ]   Size:                [ 5 ]   PrintAVLTree:       ▐" << endl;
    cout << "▐\t" << "[ 2 ]   Search:              [ 6 ]   Clear:              ▐" << endl;
    cout << "▐\t" << "[ 3 ]   Insert:              [ 7 ]   AVLify:             ▐" << endl;
    cout << "▐\t" << "[ 4 ]   Remove:              [ 0 ]   ➸ EXIT              ▐" << endl;
    cout << "▐\t" << "                                                         ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " \n ➸ Choose an option ➸  ";
    cin >> option;

    return option;
}

