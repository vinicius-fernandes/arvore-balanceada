/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Node.h
 * Author: vinicius
 *
 * Created on 31 de Maio de 2018, 08:58
 */

#ifndef NODE_H
#define NODE_H

/**
 * AVL Tree Node
 * 
 * This class is implemented to work with keys of type int. However, this class
 * can be easily adapted to work with templates. Retained in this form only for
 * greater didactics.
 */
class Node {
public:

    /**
     * Default constructor
     */
    Node();

    /**
     * Constructs a new node from a key
     * @param key
     */
    Node(int key);

    /**
     * Default destructor
     */
    virtual ~Node();

    // key associated with the node
    int key;

    // Balancing the node
    int balance;

    // left subtree
    Node* left;

    // right subtree
    Node* right;

};

#endif /* NODE_H */

