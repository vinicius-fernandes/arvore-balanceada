/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BinaryTree.cpp
 * Author: vinicius
 * 
 * Created on 31 de Maio de 2018, 17:50
 */

#include "BinaryTree.h"

BinaryTree::BinaryTree() {
    this->root = NULL;
}

BinaryTree::~BinaryTree() {
    this->Clear();
}

void BinaryTree::Clear() {
    delete this->root;
    this->root = NULL;
}

int BinaryTree::Search(int key) {
    return this->search(key, this->root);
}

int BinaryTree::search(int key, Node* &node) {

    int f = 0;

    if (node == NULL)
        f = 0;
    else if (key == node->key)
        f = 1;
    else if (key < node->key) {

        if (node->left == NULL)
            f = 2;
        else {
            node = node->left;
            f = this->search(key, node);
        }

    } else {

        if (node->right == NULL)
            f = 3;
        else {
            node = node->right;
            f = this->search(key, node);
        }
    }

    return f;
}

bool BinaryTree::Insert(int key) {
    return (this->insert(key, this->root));
}

bool BinaryTree::insert(int key, Node* &node) {

    bool ok;
    int f;
    Node* pt;
    Node* aux;

    ok = true;
    aux = node;

    f = this->search(key, aux);

    if (f == 1)
        ok = false;
    else {

        pt = new Node(key);
        pt->left = NULL;
        pt->right = NULL;

        if (f == 0)
            root = pt;
        else if (f == 2)
            aux->left = pt;
        else
            aux->right = pt;
    }

    return ok;
}

Node* BinaryTree::Remove(int key) {
    return (this->remove(key, this->root));
}

Node* BinaryTree::remove(int key, Node* &node) {

    if (node == NULL)
        return NULL;

    if (key < node->key)
        node->left = this->remove(key, node->left);

    else if (key > node->key)
        node->right = this->remove(key, node->right);

    else {

        if (node->left == NULL && node->right == NULL) {
            node = NULL;
            delete(node);

        } else if (node->left == NULL) {
            Node* temp = node;
            node = node->right;
            delete temp;

        } else if (node->right == NULL) {
            Node* temp = node;
            node = node->left;
            delete temp;

        } else {
            int temp = this->getSuccessor(node->right);
            node->key = temp;
            node->right = this->remove(temp, node->right);
        }
    }

    return node;
}

int BinaryTree::getSuccessor(Node* &node) {
    if (node == NULL)
        return -1;

    if (node->left != NULL)
        return this->getSuccessor(node->left);

    return node->key;
}

void BinaryTree::PrintBinaryTree() {
    if (this->root != NULL)
        this->print(this->root);
    else
        cout << "Empty Tree." << endl;
}

void BinaryTree::print(Node* &node) {

    if (node != NULL) {
        this->print(node->left);
        this->visit(node);
        this->print(node->right);
    }
}

void BinaryTree::visit(Node*& node) {
    cout << node->key << ", ";
}

