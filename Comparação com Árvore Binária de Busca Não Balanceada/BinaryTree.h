/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BinaryTree.h
 * Author: vinicius
 *
 * Created on 31 de Maio de 2018, 17:50
 */

#ifndef BINARYTREE_H
#define BINARYTREE_H

#include <iostream>

#include "Node.h"

using namespace std;

class BinaryTree {
public:

    /**
     * Default constructor
     */
    BinaryTree();

    /**
     * Initializes an avl tree with a key passed by parameter
     * @param key
     */
    BinaryTree(int key);

    /**
     * Default destructor
     */
    virtual ~BinaryTree();

    /**
     * Clean the tree
     */
    void Clear();

    /**
     * Calls the private search method by passing the key that will be searched
     * @param key
     * @return 
     */
    int Search(int key);

    /**
     * Calls the method that inserts an element into the Binary tree. 
     * Accepting a key as parameter. And returning true or false
     * @param key
     * @return true or false
     */
    bool Insert(int key);

    /**
     * Calls the method that removes an element from the AVL tree
     * @param key
     */
    Node* Remove(int key);

    /**
     * Calls the method prints on screen all the elements contained in the
     * Binary tree.
     */
    void PrintBinaryTree();

private:

    // Tree root node
    Node* root;

    /**
     * inserts a new node in avl tree. Returning true if the key is inserted
     * successfully or false if not
     * @param key
     * @param &node
     * @return 
     */
    bool insert(int key, Node* &node);

    /**
     * Check if a given element is present in the tree, returning a integer,
     * if it is found (0, 1, 2 or 3)
     * 0 - Indicates that the tree is empty
     * 1 - The key is already inserted in the tree
     * 2 - The key can be inserted into the left subtree
     * 3 - The key can be inserted into the right subtree
     * @param key
     * @param &node
     * @return 
     */
    int search(int key, Node* &node);

    /**
     * Removes a node from the tree and returns it
     * @param key
     * @param &node
     * @return 
     */
    Node* remove(int key, Node* &node);

    /**
     * Go through the tree on the left until you find the last node.
     * Returns the successor of node.
     * @param &node
     * @return 
     */
    int getSuccessor(Node* &node);

    /**
     * Private method prints on screen all the elements contained in the AVL
     * tree, from a root node.
     * @param &node
     */
    void print(Node* &node);

    /**
     * Prints the value of a node
     * @param &node
     */
    void visit(Node* &node);

};

#endif /* BINARYTREE_H */

